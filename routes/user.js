const express = require('express');
const router = express.Router();
const userController = require('./../controllers/user');
const auth = require('./../auth');
 

router.post('/', (req, res) => {
	userController.register(req.body).then(result => res.send(result))
})

router.post('/email-exists', (req, res) => {
	userController.emailExists(req.body).then(result => res.send(result))
})

router.post('/login', (req,res) =>{
	// userController.login(req.body).then(result => res.send(result))
	userController.login(req,res)
})
router.post('/enroll', auth.verify,(req,res) => {
	// userId = get this in token
	// courseId = body
	const params = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
	}

	userController.enroll(params).then( result => res.send(result))
})

router.get('/', (req,res) => {

	console.log("hello, this route has been triggered from an app.")
	
	userController.sample().then( result => res.send( result ))

})


router.get('/details', auth.verify, (req,res) => {
	const decodedToken = auth.decode(req.headers.authorization)
	userController.get({ userId : decodedToken.id}).then( user => res.send( user))
})

router.get('/details/:id', (req,res) => {
	userController.get({userId: req.params.id}).then(user => res.send(user))
})

router.put('/', auth.verify, (req,res) => {
	userController.editProfile(req.body).then(result => res.send(result))
});


module.exports = router;

/*function Hello(message){
	const promise = new Promise
}

Hello("message");

fetch('http://localhost:8000/api/users/email-exists')
.then(res => res.json())*/