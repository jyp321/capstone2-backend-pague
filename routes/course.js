const router = require('express').Router()
const Course = require('./../models/course');
const CourseController = require('./../controllers/course');
const auth = require('./../auth');

// get all courses
router.get('/', (req,res)=> {
	CourseController.getAll().then(courses => res.send(courses));
}); 
 
// get course by id
router.get('/:id', (req,res)=> {
	CourseController.get(req.params.id).then( result => res.send(result))
});

// add course
router.post('/', auth.verify, (req,res)=> {
	// const course = {
	// 	name: req.body.name,
	// 	description: req.body.description
	// 	price: req.body.price
	// }
	CourseController.add(req.body).then( result => res.send(result))
});

// update course
router.put('/', auth.verify, (req,res) => {
	CourseController.update(req.body).then( result => res.send(result))
});

router.put('/:id', auth.verify, (req,res) => {
	CourseController.unarchive(req.params.id).then( result => res.send(result))
})


// delete course
router.delete('/:id', auth.verify, (req,res) => {
	CourseController.archive(req.params.id).then( result => res.send(result))
})

// router.delete('/:id', auth.verify, (req,res) => {
// 	CourseController.unarchive(req.params.id).then( result => res.send(result))
// })


module.exports = router;