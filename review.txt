MVC - Models, View, Controller and Routes

Model - determines the schema which is the blueprint of our documents in a table/collection.
View - is what our user sees.
Controllers - it handles the logic and flow of our backend. This is 			  also where we can modify or create documents.
Routes - This is where we send our requests and receive our 
		 responses through.